package gitlib

import "time"

// TimeFrame defines a period of time.
type TimeFrame struct {
	After  *time.Time
	Before *time.Time
}

// Base defines the structure of a gitlib object. All objects should implement
// this interface to allow client configuration.
type Base interface {
	SetClient(interface{}) error
	fetchDataFromAPI() error
}
