package gitlib

import (
	"context"
	"errors"
	"io/ioutil"

	"github.com/google/go-github/github"
	gitlab "github.com/xanzy/go-gitlab"
	ghWebhook "gopkg.in/go-playground/webhooks.v3/github"
	git "gopkg.in/src-d/go-git.v4"
)

// RepositoryWebhookEvent defines the type of webhook events.
type RepositoryWebhookEvent uint

// The types of webhook events are listed below.
const (
	Push RepositoryWebhookEvent = iota
	Issue
	MergeRequest
	IssueComment
	CommitComment
	MergeRequestComment
	CIBuild
)

// IRepository defines the utilities of a repository.
type IRepository interface {
	ListWebhooks() ([]string, error)
	RegisterWebhook(url, secretKey string, events []RepositoryWebhookEvent) error
	UnregisterWebhook(url string) error

	ListIssues() ([]interface{}, error)
	GetIssue(number uint) (interface{}, error)
	CreateIssue(title string, body string) (interface{}, error)
	SearchIssues(created *TimeFrame, updated *TimeFrame, state *string) ([]interface{}, error)

	ListPullRequests() ([]interface{}, error)
	GetPullRequest(number uint) (interface{}, error)
	CreatePullRequest(title, body, base, head string, targetProject string) (interface{}, error)
	SearchPullRequests(created *TimeFrame, updated *TimeFrame, state *string) ([]interface{}, error)

	CreateLabel(name, color, description string, labelType string) error
	DeleteLabel(name string) error
	ListLabels() ([]string, error)

	ListCommits() ([]interface{}, error)
	SearchCommits(author string) ([]interface{}, error)

	GetPermissionLevel(user string) (string, error)

	Clone() (*git.Repository, string, error)
	Fork(namespace string) (Repository, error)
	Delete() error

	GetID() (int64, error)
	GetParent() (interface{}, error)
	RootOrg() (interface{}, error)
}

// Repository provides the actual implementation for the repository interface.
// Namespace is the owner name or group name of a repository on GitHub. On
// GitLab, it is the full path to repository excluding the name of repository
// itself.
type Repository struct {
	Namespace string
	Name      string
	payload   interface{}
	client    interface{}
	apiData   interface{}
}

// SetClient configures the client to be used with the repository API.
func (r *Repository) SetClient(c interface{}) error {
	switch client := c.(type) {
	case gitlab.Client, github.Client:
		r.client = client
		return nil
	}
	return &ErrUnknownClient{}
}

func (r *Repository) fetchDataFromAPI() error {
	switch r.apiData.(type) {
	case github.Repository, gitlab.Project:
		return nil
	}

	switch client := r.client.(type) {
	case github.Client:
		repo, _, err := client.Repositories.Get(
			context.Background(), r.Namespace, r.Name)
		if err != nil {
			return err
		}
		r.apiData = *repo
	case gitlab.Client:
		repo, _, err := client.Projects.GetProject(
			r.Namespace + "/" + r.Name)
		if err != nil {
			return err
		}
		r.apiData = *repo
	}

	return nil
}

func (r *Repository) ListWebhooks() ([]string, error) {
	panic("not implemented")
}

func (r *Repository) RegisterWebhook(url string, secretKey string, events []RepositoryWebhookEvent) error {
	panic("not implemented")
}

func (r *Repository) UnregisterWebhook(url string) error {
	panic("not implemented")
}

func (r *Repository) ListIssues() ([]interface{}, error) {
	panic("not implemented")
}

func (r *Repository) GetIssue(number uint) (interface{}, error) {
	panic("not implemented")
}

func (r *Repository) CreateIssue(title string, body string) (interface{}, error) {
	panic("not implemented")
}

func (r *Repository) SearchIssues(created *TimeFrame, updated *TimeFrame, state *string) ([]interface{}, error) {
	panic("not implemented")
}

func (r *Repository) ListPullRequests() ([]interface{}, error) {
	panic("not implemented")
}

func (r *Repository) GetPullRequest(number uint) (interface{}, error) {
	panic("not implemented")
}

func (r *Repository) CreatePullRequest(title string, body string, base string, head string, targetProject string) (interface{}, error) {
	panic("not implemented")
}

func (r *Repository) SearchPullRequests(created *TimeFrame, updated *TimeFrame, state *string) ([]interface{}, error) {
	panic("not implemented")
}

func (r *Repository) CreateLabel(name string, color string, description string, labelType string) error {
	panic("not implemented")
}

func (r *Repository) DeleteLabel(name string) error {
	panic("not implemented")
}

func (r *Repository) ListLabels() ([]string, error) {
	panic("not implemented")
}

func (r *Repository) ListCommits() ([]interface{}, error) {
	panic("not implemented")
}

func (r *Repository) SearchCommits(author string) ([]interface{}, error) {
	panic("not implemented")
}

func (r *Repository) GetPermissionLevel(user string) (string, error) {
	panic("not implemented")
}

func (r *Repository) getCloneURL() (string, error) {
	switch p := r.payload.(type) {
	case ghWebhook.RepositoryPayload:
		return p.Repository.CloneURL, nil
	}
	if err := r.fetchDataFromAPI(); err != nil {
		return "", err
	}
	switch d := r.apiData.(type) {
	case github.Repository:
		return d.GetCloneURL(), nil
	case gitlab.Project:
		return d.HTTPURLToRepo, nil
	}
	return "", errors.New("could not clone URL from the API")
}

// Clone clones the git repository and returns the *git.Repository object,
// path to the directory and errors, if any, during the cloning process.
// TODO: Implement support for cloning via OAuth tokens and SSH Keys.
func (r *Repository) Clone() (*git.Repository, string, error) {
	url, err := r.getCloneURL()
	if err != nil {
		return nil, "", err
	}
	path, err := ioutil.TempDir("/tmp", "clone")
	if err != nil {
		return nil, "", err
	}
	repo, err := git.PlainClone(path, false, &git.CloneOptions{
		URL: url,
	})
	return repo, path, err
}

func (r *Repository) Fork(namespace string) (Repository, error) {
	panic("not implemented")
}

func (r *Repository) Delete() error {
	panic("not implemented")
}

// GetID retrieves the unique identifier of the repository that does not change
// even as the names were changed.
func (r *Repository) GetID() (int64, error) {
	switch p := r.payload.(type) {
	case ghWebhook.RepositoryPayload:
		return p.Repository.ID, nil
	}
	if err := r.fetchDataFromAPI(); err != nil {
		return 0, err
	}
	switch d := r.apiData.(type) {
	case github.Repository:
		return d.GetID(), nil
	case gitlab.Project:
		return int64(d.ID), nil
	}
	return 0, errors.New("could not retrieve ID from the API")
}

func (r *Repository) GetParent() (interface{}, error) {
	panic("not implemented")
}

func (r *Repository) RootOrg() (interface{}, error) {
	panic("not implemented")
}
