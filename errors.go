package gitlib

import "fmt"

// ErrUnknownClient tells that the client sent is incorrect.
type ErrUnknownClient struct {
	c interface{}
}

func (e ErrUnknownClient) Error() string {
	return fmt.Sprintf(
		"unknown client configuration: %v, should be one of "+
			"\"github.com/google/go-github/github\".Client or "+
			"\"github.com/xanzy/go-gitlab\".Client",
		e.c)
}
