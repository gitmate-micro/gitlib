package gitlib_test

import (
	"os"
	"testing"

	vcr "github.com/ad2games/vcr-go"
	"github.com/google/go-github/github"
	gitlab "github.com/xanzy/go-gitlab"
	"gitlab.com/gitmate-micro/gitlib"
	git "gopkg.in/src-d/go-git.v4"
)

func TestGitHubRepositoryGetID(t *testing.T) {
	vcr.Start("repo.github.GetID", nil)
	defer vcr.Stop()

	repo := gitlib.Repository{Namespace: "gitmate-test-user", Name: "test"}
	repo.SetClient(*github.NewClient(nil))
	id, err := repo.GetID()
	var want int64 = 49558751

	if err != nil {
		t.Error(err)
	}
	if id != want {
		t.Errorf("have: %d, want: %d", id, want)
	}
}

func TestGitLabRepositoryGetID(t *testing.T) {
	vcr.Start("repo.gitlab.GetID", nil)
	defer vcr.Stop()

	repo := gitlib.Repository{Namespace: "gitmate-test-user", Name: "test"}
	repo.SetClient(*gitlab.NewClient(nil, ""))
	id, err := repo.GetID()
	var want int64 = 3439658

	if err != nil {
		t.Error(err)
	}
	if id != want {
		t.Errorf("have: %d, want: %d", id, want)
	}
}

func TestGitHubRepositoryClone(t *testing.T) {
	vcr.Start("repo.github.Clone", nil)
	defer vcr.Stop()

	repo := gitlib.Repository{Namespace: "gitmate-test-user", Name: "test"}
	repo.SetClient(*github.NewClient(nil))
	r, dir, err := repo.Clone()
	defer os.RemoveAll(dir)
	if err != nil {
		t.Error(err)
	}
	remote, err := r.Remote("origin")
	if err != nil {
		t.Error(err)
	}
	remotes, err := remote.List(&git.ListOptions{})
	if err != nil {
		t.Error(err)
	}
	if remotes == nil {
		t.Errorf("cloning failed, no remotes found.")
	}
}

func TestGitLabRepositoryClone(t *testing.T) {
	vcr.Start("repo.gitlab.Clone", nil)
	defer vcr.Stop()

	repo := gitlib.Repository{Namespace: "gitmate-test-user", Name: "test"}
	repo.SetClient(*gitlab.NewClient(nil, ""))
	r, dir, err := repo.Clone()
	defer os.RemoveAll(dir)
	if err != nil {
		t.Error(err)
	}
	remote, err := r.Remote("origin")
	if err != nil {
		t.Error(err)
	}
	remotes, err := remote.List(&git.ListOptions{})
	if err != nil {
		t.Error(err)
	}
	if remotes == nil {
		t.Errorf("cloning failed, no remotes found.")
	}
}
